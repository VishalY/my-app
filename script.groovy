def BuildJar() {
    echo "building the application..."
    sh 'mvn package'
} 

def Test() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'dh-credentials',passwordVariable: 'PASS' ,usernameVariable: 'USER')]){
                        sh 'docker build -t vishhal3/repo-javamvn:jma-2.2 .'
                        sh " echo $PASS | docker login -u $USER --password-stdin"
                        sh ' docker push vishhal3/repo-javamvn:jma-2.2'
    }
} 

def Deploy() {
    echo "deploying the application..."
} 

return this
